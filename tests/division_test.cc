#include "division_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(divisionTest);

void divisionTest::setUp() {}

void divisionTest::tearDown() {}

void divisionTest::division_normal()
{
  dividende = 4;
  diviseur = 2;
  CPPUNIT_ASSERT_EQUAL(static_cast<double>(2), static_cast<double>(arithmetique::division(dividende, diviseur)));

  dividende = -4;
  diviseur = -2;
  CPPUNIT_ASSERT_EQUAL(static_cast<double>(2), static_cast<double>(arithmetique::division(dividende, diviseur)));
  dividende = 4;
  diviseur = 1;
  CPPUNIT_ASSERT_EQUAL(static_cast<double>(dividende), static_cast<double>(arithmetique::division(dividende, diviseur)));
  dividende = -4;
  diviseur = -1;
  CPPUNIT_ASSERT_EQUAL(static_cast<double>(4), static_cast<double>(arithmetique::division(dividende, diviseur)));
}

void divisionTest::division_max()
{
  dividende = std::numeric_limits<int>::max();
  diviseur = 2;
  CPPUNIT_ASSERT_LESS(static_cast<double>(dividende), static_cast<double>(arithmetique::division(dividende, diviseur)));
}

void divisionTest::division_min()
{
  dividende = std::numeric_limits<int>::min();
  diviseur = -2;
  CPPUNIT_ASSERT_GREATER(static_cast<double>(dividende), static_cast<double>(arithmetique::division(dividende, diviseur)));
}
void divisionTest::division_zero()
{
  dividende = 2;
  diviseur = 0;

  //dividende = 2/diviseur;
  arithmetique::division(dividende, diviseur);
//  CPPUNIT_ASSERT_THROW(thrown, std::exception);
  // try{
  //arithmetique::division(dividende, diviseur);
  //}
  // catch(...){
  //  CPPUNIT_ASSERT(true);
     //throw;
     // }
  //CPPUNIT_ASSERT(true);
}
